import csv
import gzip
import sys
from pathlib import Path


def _save_gzip(path: Path, name_without_extension: str, extension: str, data):
    print('Re-zipping GZIP file...')

    filename = f'{name_without_extension}_updated.csv{extension}'

    path.parent.joinpath(filename)
    with gzip.open(path.parent.joinpath(filename), 'wt') as f:
        writer = csv.writer(f)
        print(f'Writing file {filename} to location {path.parent}...')
        writer.writerows(data)
    print('Done!')


def print_top(path: str):
    p = Path(path)

    with gzip.open(p, 'rt') as f:
        reader = csv.reader(f)
        for _ in range(10):
            print(next(reader))


def modify_file(path: str, new_data: str):
    p = Path(path)
    name_without_extension = p.stem.split('.')[0]
    ext = p.suffix

    print("Adding column with value {} to file {}".format(new_data, name_without_extension))

    print('Unzipping GZIP file...')
    with gzip.open(p, 'rt') as f:
        reader = csv.reader(f)
        data = list(reader)

    print('Adding column...')
    for row in data:
        row.append(new_data)

    _save_gzip(p, name_without_extension, ext, data)


def delete_column(path: str, column_number: int):
    p = Path(path)
    name_without_extension = p.stem.split('.')[0]
    ext = p.suffix
    print('Deleting column {} from file {}...'.format(column_number, name_without_extension))

    print('Unzipping GZIP file...')
    with gzip.open(p, 'rt') as f:
        reader = csv.reader(f)
        data = list(reader)

    print('Deleting column...')
    for row in data:
        if column_number <= len(row):
            row.pop(column_number - 1)

    _save_gzip(p, name_without_extension, ext, data)


if __name__ == "__main__":
    # Check if correct number of arguments are provided

    if len(sys.argv) == 1:
        print("Usage: python modify_csv.py <command> <path_to_file> <new_data>")
        sys.exit(1)

    command = sys.argv[1]

    if command == 'top':
        input_file = sys.argv[2]
        print_top(input_file)
        sys.exit(0)
    elif command == 'modify':
        if len(sys.argv) < 4:
            print("Usage: python modify_csv.py modify <path_to_file> <new_data>")
            sys.exit(1)
        else:
            input_file = sys.argv[2]
            new_column_data = sys.argv[3]
            modify_file(input_file, new_column_data)
    elif command == 'delete':
        if len(sys.argv) < 4:
            print("Usage: python modify_csv.py delete <path_to_file> <column number>")
        else:
            input_file = sys.argv[2]
            column_number = int(sys.argv[3])
            delete_column(input_file, column_number)
    elif command == 'help':
        print('CSV file modification tool. Expects all files to be GZIP-ed CSV files.')
        print('Can be used to preview a file, or append a new column with a specified value.')
        print("Usage: python modify_csv.py <command> <path_to_file> <new_data>")
        print("Commands:")
        print("top - Print top 10 rows of the CSV file")
        print("modify - Add new column to the CSV file")
        print("delete - Delete column from the CSV file")
        print('Version 1.2')
